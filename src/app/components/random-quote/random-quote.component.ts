import { Component, OnInit } from '@angular/core';
import { ForismaticService } from '../../services/forismatic.service';
import { Quote } from '../../classes/quote';

@Component({
  selector: 'app-random-quote',
  providers: [ ForismaticService ],
  templateUrl: './random-quote.component.html',
  styleUrls: ['./random-quote.component.scss']
})
export class RandomQuoteComponent implements OnInit {
  public quote: Quote = {
    author: 'Albert Gray',
    text: ' Winners have simply formed the habit of doing things losers don\'t like to do.'
  }
  constructor (private forismaicService: ForismaticService) { }
  ngOnInit() {
  }
  getQuote() {
    this.forismaicService.getQuote()
      .subscribe(quotes => {
        this.quote.author = quotes.quoteAuthor;
        this.quote.text = quotes.quoteText;
       }
      );
  }
}
