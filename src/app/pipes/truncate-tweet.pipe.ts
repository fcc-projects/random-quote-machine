import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncateTweet'
})
export class TruncateTweetPipe implements PipeTransform {

  transform(value: String): String {
    value = 'https://twitter.com/intent/tweet?via=P1xt&hashtags=ChinguCohorts&text=' + value;
    const num = 184;
    if (value.length > num) {
      if (value.length < 3 || num < 3) {
        value = value.substr(0, num) + '...';
      } else {
        value = value.substr(0, num - 3) + '...';
      }
    }
    return value;
  }
}
