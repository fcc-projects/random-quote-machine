import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '@angular/material';
import { HttpModule, JsonpModule } from '@angular/http';

import 'hammerjs';
import { AppComponent } from './components/app/app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { RandomQuoteComponent } from './components/random-quote/random-quote.component';
import { ForismaticService } from './services/forismatic.service';
import { TruncateTweetPipe } from './pipes/truncate-tweet.pipe';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    RandomQuoteComponent,
    TruncateTweetPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    JsonpModule,
    HttpModule
  ],
  providers: [ForismaticService],
  bootstrap: [AppComponent]
})
export class AppModule { }
